package com.example.myapplication.view;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.api.ApiInterface;
import com.example.myapplication.api.RetrofitClient;
import com.example.myapplication.model.Word;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {
    private EditText mSearchEt;
    private TextView mWordTv;
    private Word word;
    private Button mSearchBt;
    private TextView mPartOfSpeechTv;
    private TextView mDefintionTv;
    private TextView mExampleTv;
    public HomeFragment() {
        // Required empty public constructor
    }
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        mSearchEt = view.findViewById(R.id.search_et);
        mWordTv = view.findViewById(R.id.word_tv);
        mSearchBt = view.findViewById(R.id.search_bt);
        mPartOfSpeechTv = view.findViewById(R.id.partOfSpeech_tv);
        mDefintionTv = view.findViewById(R.id.definition_tv);
        mExampleTv = view.findViewById(R.id.example_tv);
        mSearchBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getWordFromApi(mSearchEt.getText().toString());
            }
        });
        return view;
    }

    private void getWordFromApi(String wordSearch) {
        ApiInterface retrofit = new RetrofitClient().getRetrofitInstance().create(ApiInterface.class);
        Call<ArrayList<Word>> call = retrofit.getWord(wordSearch);
        call.enqueue(new Callback<ArrayList<Word>>() {
            @Override
            public void onResponse(Call<ArrayList<Word>>call, Response<ArrayList<Word>> response) {
                word = response.body().get(0);
                Log.d("Tag", word.toString());
                mWordTv.setText(word.getWord());
                mPartOfSpeechTv.setText(word.getMeanings().get(0).getPartOfSpeech());
                mDefintionTv.setText(word.getMeanings().get(0).getDefinitions().get(0).getDefinition());
                mExampleTv.setText("Example: " + word.getMeanings().get(0).getDefinitions().get(0).getExample());
            }
            @Override
            public void onFailure(Call<ArrayList<Word>> call, Throwable t) {

            }
        });
    }
}