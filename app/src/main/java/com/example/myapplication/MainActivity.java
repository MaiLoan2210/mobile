package com.example.myapplication;

import android.os.Bundle;
import android.view.View;
import android.view.Menu;

import com.example.myapplication.adapters.ViewPagerAdapter;
import com.example.myapplication.view.FavouriteFragment;
import com.example.myapplication.view.HomeFragment;
import com.example.myapplication.view.ReviewFragment;
import com.example.myapplication.view.SettingFragment;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.navigation.NavigationView;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.widget.ViewPager2;

import com.example.myapplication.databinding.ActivityMainBinding;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {
    //fragment
    private HomeFragment mHomeFragment = new HomeFragment();
    private FavouriteFragment mFavouriteFragment = new FavouriteFragment();
    private ReviewFragment mReviewFragment = new ReviewFragment();
    private SettingFragment mSettingFragment = new SettingFragment();
    //Viewpager and Tab layout
    private ViewPager2 mViewPager;
    private ViewPagerAdapter mViewPagerAdapter;
    private TabLayout mTabLayout;
    private String[] mTitleList = {"Home", "Favourite", "Review", "Setting"};
    private Integer[] mIconList = {R.drawable.ic_menu_home, R.drawable.ic_menu_favorite, R.drawable.ic_menu_review,
    R.drawable.ic_menu_settings};

    private AppBarConfiguration mAppBarConfiguration;
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.appBarMain.toolbar);
        DrawerLayout drawer = binding.drawerLayout;
        NavigationView navigationView = binding.navView;
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_favourite, R.id.nav_review, R.id.nav_setting)
                .setOpenableLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
        getSupportActionBar().setTitle("My Dictionary");
        setUpTab();
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
//        return true;
//    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    private void setUpTab() {
        mViewPager = findViewById(R.id.view_pager);
        mViewPagerAdapter = new ViewPagerAdapter(this);
        mViewPagerAdapter.addFragment(mHomeFragment);
        mViewPagerAdapter.addFragment(mFavouriteFragment);
        mViewPagerAdapter.addFragment(mReviewFragment);
        mViewPagerAdapter.addFragment(mSettingFragment);
        mViewPager.setAdapter(mViewPagerAdapter);
        mViewPager.setOffscreenPageLimit(4);
        mTabLayout = findViewById(R.id.tab_layout);
        new TabLayoutMediator(mTabLayout, mViewPager,
                (tab, position) ->
                {
                    tab.setText(mTitleList[position]);
                    tab.setIcon(mIconList[position]);
                }
        ).attach();
    }


}