package com.example.myapplication.api;

import com.example.myapplication.Constant.ApiConstant;
import com.example.myapplication.model.Word;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private Retrofit retrofit;
    public static Word word;
    public Retrofit getRetrofitInstance() {
        retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstant.BaseURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }
}
