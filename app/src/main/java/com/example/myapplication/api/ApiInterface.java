package com.example.myapplication.api;

import com.example.myapplication.model.Word;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface ApiInterface {
    @GET("v2/entries/en/{word}")
    Call<ArrayList<Word>>getWord(@Path("word") String word);
}