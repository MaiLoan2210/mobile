package com.example.myapplication.model;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Word {
    @SerializedName("word")
    private String word;
    @SerializedName("phonetic")
    private String phonetic;
    @SerializedName("meanings")
    private ArrayList<Meaning> meanings;
    public String getPhonetic() {
        return phonetic;
    }

    public ArrayList<Meaning> getMeanings() {
        return meanings;
    }
    public void setMeanings(ArrayList<Meaning> meanings) {
        this.meanings = meanings;
    }

    public String getWord() {
        return word;
    }

    public void setPhonetic(String phonetic) {
        this.phonetic = phonetic;
    }

    public void setWord(String word) {
        this.word = word;
    }
}
